package gad.denunciasalud;

import android.app.Application;

import com.parse.Parse;

/**
 * Created by gabriel on 24/7/16.
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                        .applicationId("7Myg258h5XS4oi1n6sG4U4VIgu1bFIo0")
                        .server("http://appdenuncia.herokuapp.com/parse/").build()
        );
    }
}
