package gad.denunciasalud;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import com.parse.ParseException;
import com.parse.SaveCallback;

import gad.denunciasalud.models.Post;


public class FormActivity extends AppCompatActivity {


    private Post post;
    private EditText inputName;

//    @NotEmpty
//    @Email
    private EditText inputMail;
    private EditText inputNumber;
    private Switch switchConfidential;
    private FloatingActionButton btnSend;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        inputName = (EditText) findViewById(R.id.input_name);
        inputMail = (EditText) findViewById(R.id.input_mail);
        inputNumber = (EditText) findViewById(R.id.input_number);
        switchConfidential = (Switch) findViewById(R.id.form_confidencial);
        btnSend = (FloatingActionButton) findViewById(R.id.enviar);

        btnSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(validate()){
                    savePost();

                }

            }
        });

        post = new Post(getIntent().getExtras());

    }

    private boolean isEmpty(EditText myeditText) {
        return myeditText.getText().toString().trim().length() == 0;
    }

    private boolean validate(){
        boolean valid = true;
        if(isEmpty(inputName)){
            inputName.setError("El nombre no puede quedar vacio");
            valid = false;
        }
        if(isEmpty(inputMail)){
            inputMail.setError("El correo no puede quedar vacio");
            valid = false;
        }
        if(isEmpty(inputNumber)){
            inputNumber.setError("El número de teléfono no puede quedar vacio");
            valid = false;
        }
        return valid;
    }

    private void savePost(){

        post.setName(inputName.getText().toString());
        post.setMail(inputMail.getText().toString());
        post.setPhone(inputNumber.getText().toString());
        post.setConfidential(switchConfidential.isChecked());

        dialog = ProgressDialog.show(this, "", "Espera un momento, estamos subiendo tu denuncia.", true);

        post.savePost(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                if(e == null){

                    dialog.dismiss();

                    Intent intent = new Intent(FormActivity.this, MapActivity.class);
                    if(post.toBundle() != null){
                        intent.putExtras(post.toBundle());
                    }
                    startActivity(intent);
                    finish();
                }

            }
        });


    }
}
