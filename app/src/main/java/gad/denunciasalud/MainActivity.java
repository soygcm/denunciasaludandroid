package gad.denunciasalud;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;
import android.widget.VideoView;


import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
//import com.google.android.gms.appindexing.Action;
//import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.SaveCallback;

import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import gad.denunciasalud.fragments.CaptureFragment;
import gad.denunciasalud.fragments.CategoryFragment;
import gad.denunciasalud.fragments.DescriptionFragment;
import gad.denunciasalud.fragments.FaceFragment;
import gad.denunciasalud.models.Post;


public class MainActivity extends AppCompatActivity {

    private static final int PLACE_PICKER_REQUEST = 59049;
    private static final int FORM_MS = 3992;
    private static final int REQUEST_CAMERA = 8857;
    private Camera mCamera;
    private CameraPreview mPreview;
    private MediaRecorder mMediaRecorder;

    private FaceFragment faceFragment;

    FrameLayout preview;

    Post post;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private ProgressDialog dialog;

    SharedPreferences prefs = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);

        }

        setContentView(R.layout.camera_surface);

        prefs = getSharedPreferences("gad.denunciasalud", MODE_PRIVATE);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        post = new Post();
        post.setActivity(this);
        post.setOs("android");

        showCaptureFragment();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        post.callbackManager(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);

                post.setLocation(new ParseGeoPoint(place.getLatLng().latitude, place.getLatLng().longitude));


                if(faceFragment != null){

                    faceFragment.changePlace(place.getName());

                }

            }
        }

    }

    private void stopRecording() {

        post.setVideoURL(videoPath);

        // stop recording and release camera
        mMediaRecorder.stop();  // stop the recording
        releaseMediaRecorder(); // release the MediaRecorder object
        mCamera.lock();         // take camera access back from MediaRecorder

        // inform the user that recording has stopped
//                            setCaptureButtonText("Capture");
        isRecording = false;

        mCamera.stopPreview();

        recordingFinish = true;

        playVideo();

        showFaceFragment();

    }

    private void showMapActivity() {

        //send position new denuncia para poder hacerle el higlight

        Intent intent = new Intent(this, MapActivity.class);
        if(post.toBundle() != null){
            intent.putExtras(post.toBundle());
        }
        startActivity(intent);
        finish();

    }

    private void showCaptureFragment() {
        CaptureFragment fragment = new CaptureFragment();

        fragment.setOnCaptureSelectedLister(new CaptureFragment.onCaptureSelectedListener() {
            @Override
            public void onClickCapture() {

                if (prepareVideoRecorder()) {

                    startRecording();

                } else {
                    // prepare didn't work, release the camera
                    releaseMediaRecorder();
                    // inform user
                }

            }

            @Override
            public void onShow() {

                if(recordingFinish){

                    videoView.setVisibility(View.GONE);
                    preview.setVisibility(View.VISIBLE);
                    mPreview.setVisibility(View.VISIBLE);

                    stopVideo();
                    recordingFinish = false;


                    startCameraPreview();

//                    mCamera.startPreview();

                    Log.d("DS", "if(recordingFinish)");

                }

//                Toast.makeText(MainActivity.this, "onShow", Toast.LENGTH_LONG).show();

            }

            @Override
            public void onClickViewMap() {
                showMapActivity();
            }
        });


        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentContainer, fragment, "capture")
                .commit();

    }

    private Post.OnPostListener postListener(){
        return new Post.OnPostListener() {

//            @Override
//            public void onSavePost() {
//
//            }

            @Override
            public void onUploadVideo() {

                post.setActive(true);

                post.savePost(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {

                        post.loginAndPost();


                    }
                });



            }

            @Override
            public void onDownloadVideo(File file) {

            }

            @Override
            public void onPostToFB() {

                dialog.dismiss();

                showNextActivity();


            }

            @Override
            public void onUploadProgressChange(int percentage) {

                if (descriptionFragment != null){

                    descriptionFragment.setProgressBar(percentage);

                }

            }

            @Override
            public void onDownloadProgressChange(int percentage) {

            }
        };
    }

    private void showNextActivity(){

        if(post.isSendToMS()){
            Intent intent = new Intent(this, FormActivity.class);

            Bundle bundle = post.toBundle();
            intent.putExtras(bundle);
            startActivityForResult(intent, FORM_MS);
            finish();
        }else{
            showMapActivity();
        }

    }

    private DescriptionFragment descriptionFragment;

    private void showDescriptionFragment() {

        Bundle bundle = new Bundle();
        bundle.putString("perception", post.getPerception());
        bundle.putString("category", post.getCategoryName());

        descriptionFragment = new DescriptionFragment();
        descriptionFragment.setArguments(bundle);


        descriptionFragment.setOnSavePostLister(new DescriptionFragment.onSavePostListener() {
            @Override
            public void onSavePost(String description, Boolean sendToMS, Boolean sendToFB) {


                post.setDescriptionText(description);
                post.setSendToMS(sendToMS);
                post.setShareOnFacebook(sendToFB);

                post.setOnPostListener(postListener());

                post.saveAndUploadVideo();

                dialog = ProgressDialog.show(MainActivity.this, "",
                        "Espera un momento, estamos subiendo tu denuncia.", true);

            }
        });

        replaceFragment(R.id.fragmentContainer, descriptionFragment, "description", "back");
    }

    private void showCategoryFragment() {

        Bundle bundle = new Bundle();
        bundle.putString("perception", post.getPerception());
        CategoryFragment categoryFragment = new CategoryFragment();
        categoryFragment.setArguments(bundle);

        categoryFragment.setCategoryLister(new CategoryFragment.CategoryListener() {
            @Override
            public void onCategorySelected(String category) {

                post.setCategoryName(category);

                showDescriptionFragment();
            }
        });

        replaceFragment(R.id.fragmentContainer, categoryFragment, "category", "back");

    }

    private void showFaceFragment() {

        faceFragment = new FaceFragment();
        faceFragment.setOnFaceSelectedLister(new FaceFragment.onFaceSelectedListener() {
            @Override
            public void onFaceSelected(String face) {

                post.setPerception(face);

                showCategoryFragment();
            }

            @Override
            public void onDirectionClick() {

                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                try {
                    startActivityForResult( builder.build(MainActivity.this ), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

            }
        });

        View container = (View) findViewById(R.id.fragmentContainer);

        container.bringToFront();

        replaceFragment(R.id.fragmentContainer, faceFragment, "face", "back");

    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }

    VideoView videoView;

    CircularProgressBar circularProgressBar;

    private void startRecording() {

        // Camera is available and unlocked, MediaRecorder is prepared,
        // now you can start recording
        mMediaRecorder.start();

        // inform the user that recording has started
//                                setCaptureButtonText("Stop");

        isRecording = true;

        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                // update circle progress
            }

            public void onFinish() {
                stopRecording();
            }
        }.start();
    }

    private String videoPath = "";

    private void playVideo() {

        if (recordingFinish){

            videoView = (VideoView) findViewById(R.id.view_video);
            videoView.setVisibility(View.VISIBLE);
            preview.setVisibility(View.GONE);
            mPreview.setVisibility(View.GONE);
            videoView.setVideoPath(videoPath);
            videoView.start();

            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                }
            });

        }

    }

    private void stopVideo(){

        if(videoView != null){
            videoView.stopPlayback();
        }

    }

    void firstRun(){
        if (prefs.getBoolean("firstrun", true)) {

            prefs.edit().putBoolean("firstrun", false).commit();

            Intent intent = new Intent(this, WelcomeActivity.class);
            startActivity(intent);
            finish();

        }
    }

    protected void onResume() {
        super.onResume();

//        prefs.getBoolean("firstrun", true)

        firstRun();

        showCamera();

        playVideo();

    }

    void showCamera(){
        if(!recordingFinish && this.mCamera == null){

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        ){

                    startCameraPreview();
                } else {

                    if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)){
                        Toast.makeText(this, "Para poder grabar denuncias, es necesario tener el permiso de usar la camara del dispositivo.", Toast.LENGTH_SHORT).show();
                    }

                    shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO);

                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);

                }
            }else{
                startCameraPreview();
            }


        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CAMERA){

            if(grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED
                    && grantResults[3] == PackageManager.PERMISSION_GRANTED){
                startCameraPreview();
            }else{
                Toast.makeText(this, "no diste permiso para acceder a la camar", Toast.LENGTH_SHORT).show();
            }



        }else{
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event

        stopVideo();

    }

    private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.setPreviewCallback(null);
            mPreview.getHolder().removeCallback(mPreview);
            mCamera.release();        // release the camera for other applications

            mCamera = null;
        }
    }

    private void startCameraPreview(){
        mCamera = getCameraInstance();
        // Create our Preview view and set it as the content of our activity.

        if(mCamera != null){
            mPreview = new CameraPreview(this, mCamera);
            preview = (FrameLayout) findViewById(R.id.camera_preview);
            preview.addView(mPreview);
        }
    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
//            if (Camera.getNumberOfCameras() >= 2) {
            //if you want to open front facing camera use this line
//                c = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
//            }else{

            c = Camera.open(); // attempt to get a Camera instance
//            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error", "cuantas veces " + e.getLocalizedMessage());
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    private boolean isRecording = false;

    private boolean recordingFinish = false;

    private boolean prepareVideoRecorder() {



        mMediaRecorder = new MediaRecorder();

        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);

        // Step 2: Set sources
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
        mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_480P));
        mMediaRecorder.setMaxDuration(5000);
        mMediaRecorder.setOrientationHint(90);

        // Step 4: Set output
        videoPath = getOutputMediaFile(MEDIA_TYPE_VIDEO).toString();

        mMediaRecorder.setOutputFile(videoPath);


        // Step 5: Set the preview output
        mMediaRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

        // Step 6: Prepare configured MediaRecorder
        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            Log.d("DS", "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            Log.d("DS", "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /**
     * Create a file Uri for saving an image or video
     */
    private static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Create a File for saving an image or video
     */
    public static File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        Boolean isSDPresent = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        File enviroment = null;
        if (isSDPresent) {
            enviroment = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_MOVIES);
        } else {
            enviroment = Environment.getDataDirectory();
        }

        File mediaStorageDir = new File(enviroment, "DenunciaSalud");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("DenunciaSalud", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VIDDS_" + timeStamp + ".mp4");
        } else {
            return null;
        }

//        videoPath =

//        Log.d("DenunciaSalud", mediaFile.getPath().toString());

        return mediaFile;
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onStop() {
        super.onStop();


    }
}

