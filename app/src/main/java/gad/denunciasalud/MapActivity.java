package gad.denunciasalud;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.Console;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gad.denunciasalud.models.Post;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int REQUEST_LOCATION = 9956;
    private ImageButton btnCamera;

    private Location mLastLocation;

    private void centerMap() {

        mGoogleApiClient.connect();


    }

    private void centrarAhoraSi() {


//        currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

//        if (currentLocation == null) {

//            map.animateCamera(CameraUpdateFactory.newLatLngZoom(
//                    new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

//        }

        getPosts();

    }

    private LocationManager locationManager;

    GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

//        new GoogleApiClient()

        if (mGoogleApiClient == null) {
            // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(AppIndex.API).build();
        }

        btnCamera = (ImageButton) findViewById(R.id.btnCamera);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MapActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_LOCATION) {


            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                centerMap();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                map.setMyLocationEnabled(true);

            } else {
                Toast.makeText(this, "no diste permiso para acceder a tu ubicación.", Toast.LENGTH_SHORT).show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }

    }

    private HashMap<Marker, Post> posts = new HashMap<>();


    private void getPosts() {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Post");
        query.setLimit(1000);
        query.whereExists("location");
        query.whereNear("location", new ParseGeoPoint(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
        query.whereEqualTo("active", true);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> postsParse, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + postsParse.size() + " scores");


                    for (ParseObject pObject : postsParse) {


                        Post post = new Post(pObject);

                        post.setActivity(MapActivity.this);

                        Marker marker = map.addMarker(post.getMarker());
                        posts.put(marker, post);


                    }


                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });

    }

    private GoogleMap map;

    private GoogleMap.InfoWindowAdapter infoWindowAdapter() {

        return new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                View v = getLayoutInflater().inflate(R.layout.view_infowindow, null);

                Post post = posts.get(marker);

                TextView textDescription = (TextView) v.findViewById(R.id.textDescription);
                textDescription.setText(post.getDescriptionText());

                ImageView iconCategory = (ImageView) v.findViewById(R.id.iconCategory);
                iconCategory.setImageDrawable(post.getCategoryDrawable());

                return v;
            }
        };

    }

    void getLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                centerMap();
                map.setMyLocationEnabled(true);


            } else {

                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    Toast.makeText(this, "Para poder ubicarte", Toast.LENGTH_SHORT).show();
                }

                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION);

            }
        } else {
            centerMap();
            map.setMyLocationEnabled(true);

        }


    }

    @Override
    public void onMapReady(GoogleMap map) {

        this.map = map;

        map.setPadding(0, 90, 0, 0);

        map.setInfoWindowAdapter(infoWindowAdapter());

        getLocation();


        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                showViewPostActivity(marker);

            }
        });


    }

    private void showViewPostActivity(Marker marker) {

        Post post = posts.get(marker);

        Intent intent = new Intent(this, ViewPostActivity.class);
        intent.putExtras(post.toBundle());
        startActivity(intent);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {

            centrarAhoraSi();
//           Log.d("LastLocation", String.valueOf(mLastLocation.getLatitude())) ;
//            mLatitudeText.setText(String.valueOf(mLastLocation.getLatitude()));
//            mLongitudeText.setText(String.valueOf(mLastLocation.getLongitude()));
        }
    }


    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("onConnectionFail", connectionResult.getErrorMessage());
    }
}
