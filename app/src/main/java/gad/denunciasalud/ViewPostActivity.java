package gad.denunciasalud;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

import gad.denunciasalud.models.Post;


public class ViewPostActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener{

    private Post post;
    private ProgressBar progressBar;
    private TextureView videoView;
    private MediaPlayer mMediaPlayer;


    private Post.OnPostListener onPostListener = new Post.OnPostListener() {
        @Override
        public void onUploadVideo() {

        }

        @Override
        public void onDownloadVideo(File file) {
            dialog.dismiss();
            progressBar.setVisibility(View.INVISIBLE);

            try {
                mMediaPlayer= new MediaPlayer();
                mMediaPlayer.setDataSource(file.getPath());
                mMediaPlayer.setSurface(s);
                mMediaPlayer.prepare();
//

                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.start();
                mMediaPlayer.setLooping(true);
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


//

        }

        @Override
        public void onPostToFB() {

        }

        @Override
        public void onUploadProgressChange(int percentage) {

        }

        @Override
        public void onDownloadProgressChange(int percentage) {
            progressBar.setProgress(percentage);
        }
    };

    private Boolean rotated = false;
    private ProgressDialog dialog;

    private ImageButton btnFace;
    private ImageView iconCategory;
    private TextView textDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);

        post = new Post(getIntent().getExtras());

        post.setActivity(this);

        btnFace = (ImageButton) findViewById(R.id.btnFace);
        iconCategory = (ImageView) findViewById(R.id.iconCategory);
        textDescription = (TextView) findViewById(R.id.textDescription);

        iconCategory.setImageDrawable(post.getCategoryDrawable());
        textDescription.setText(post.getDescriptionText());
        btnFace.setImageDrawable(post.getPerceptionDrawable());

        videoView = (TextureView) findViewById(R.id.view_video);

        final ViewTreeObserver observer= videoView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        if(!rotated && !post.getOs().equals("android")){
                            videoView.setPivotX(videoView.getWidth()/2);
                            videoView.setPivotY(videoView.getWidth()/2);
                            videoView.setRotation(90);
                            videoView.setLayoutParams(new FrameLayout.LayoutParams(videoView.getHeight(), videoView.getWidth()));
                            rotated = true;
                        }

                    }
                });

        Log.d("AnchoAlto", videoView.getLayoutParams().height+"");
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        videoView.setSurfaceTextureListener(this);

        post.setOnPostListener(onPostListener);



    }

    private Surface s;

    @Override
    protected void onPause() {
        super.onPause();

        if(mMediaPlayer != null){
            mMediaPlayer.stop();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mMediaPlayer != null){
            mMediaPlayer.start();
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        surface.setDefaultBufferSize(videoView.getLayoutParams().height, videoView.getLayoutParams().width);
        s = new Surface(surface);

        dialog = ProgressDialog.show(this, "", "Espera un momento, estamos descargando la denuncia.", true);

        post.downloadVideo();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }
}
