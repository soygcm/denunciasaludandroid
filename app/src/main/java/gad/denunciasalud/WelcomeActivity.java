package gad.denunciasalud;


import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.dev.sacot41.scviewpager.DotsView;
import com.dev.sacot41.scviewpager.SCPositionAnimation;
import com.dev.sacot41.scviewpager.SCViewAnimation;
import com.dev.sacot41.scviewpager.SCViewAnimationUtil;
import com.dev.sacot41.scviewpager.SCViewPager;
import com.dev.sacot41.scviewpager.SCViewPagerAdapter;


public class WelcomeActivity extends FragmentActivity {

    private static final int NUM_PAGES = 7;

    private SCViewPager mViewPager;
    private SCViewPagerAdapter mPageAdapter;
    private DotsView mDotsView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_welcome);

        mViewPager = (SCViewPager) findViewById(R.id.viewpager_main_activity);
        mDotsView = (DotsView) findViewById(R.id.dotsview_main);
        mDotsView.setDotRessource(R.drawable.dot_selected, R.drawable.dot_unselected);
        mDotsView.setNumberOfPage(NUM_PAGES);

        mPageAdapter = new SCViewPagerAdapter(getSupportFragmentManager());
        mPageAdapter.setNumberOfPage(NUM_PAGES);
        mPageAdapter.setFragmentBackgroundColor(R.color.white);
        mViewPager.setAdapter(mPageAdapter);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mDotsView.selectDot(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        final Point size = SCViewAnimationUtil.getDisplaySize(this);

        View textUno = findViewById(R.id.layout_uno);
        SCViewAnimation animUno = new SCViewAnimation(textUno);
        animUno.addPageAnimation(new SCPositionAnimation(this, 0, -size.x, 0));
        mViewPager.addAnimation(animUno);

        View textDos = findViewById(R.id.layout_dos);
        SCViewAnimation animDos = new SCViewAnimation(textDos);
        animDos.startToPosition(size.x, null);
        animDos.addPageAnimation(new SCPositionAnimation(this, 0, -size.x, 0));
        animDos.addPageAnimation(new SCPositionAnimation(this, 1, -size.x, 0));
        mViewPager.addAnimation(animDos);

        View textTres = findViewById(R.id.layout_tres);
        SCViewAnimation animTres = new SCViewAnimation(textTres);
        animTres.startToPosition(size.x, null);
        animTres.addPageAnimation(new SCPositionAnimation(this, 1, -size.x, 0));
        animTres.addPageAnimation(new SCPositionAnimation(this, 2, -size.x, 0));

        mViewPager.addAnimation(animTres);

        View textCuatro = findViewById(R.id.layout_cuatro);
        SCViewAnimation animCuatro = new SCViewAnimation(textCuatro);
        animCuatro.startToPosition(size.x, null);
        animCuatro.addPageAnimation(new SCPositionAnimation(this, 2, -size.x, 0));
        animCuatro.addPageAnimation(new SCPositionAnimation(this, 3, -size.x, 0));

        mViewPager.addAnimation(animCuatro);

        View textCinco = findViewById(R.id.layout_cinco);
        SCViewAnimation animCinco = new SCViewAnimation(textCinco);
        animCinco.startToPosition(size.x, null);
        animCinco.addPageAnimation(new SCPositionAnimation(this, 3, -size.x, 0));
        animCinco.addPageAnimation(new SCPositionAnimation(this, 4, -size.x, 0));

        mViewPager.addAnimation(animCinco);

        View textSeis = findViewById(R.id.layout_seis);
        SCViewAnimation animSeis = new SCViewAnimation(textSeis);
        animSeis.startToPosition(size.x, null);
        animSeis.addPageAnimation(new SCPositionAnimation(this, 4, -size.x, 0));
        animSeis.addPageAnimation(new SCPositionAnimation(this, 5, -size.x, 0));

        mViewPager.addAnimation(animSeis);

        View textSiete = findViewById(R.id.layout_siete);
        SCViewAnimation animSiete = new SCViewAnimation(textSiete);
        animSiete.startToPosition(size.x, null);
        animSiete.addPageAnimation(new SCPositionAnimation(this, 5, -size.x, 0));
        mViewPager.addAnimation(animSiete);

        Button startButton = (Button) findViewById(R.id.btn_start);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

    }

}