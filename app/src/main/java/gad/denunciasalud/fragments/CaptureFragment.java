package gad.denunciasalud.fragments;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import gad.denunciasalud.R;

public class CaptureFragment extends Fragment {

    private ProgressBar progressBar;

    public interface onCaptureSelectedListener{
        void onClickCapture();
        void onClickViewMap();
        void onShow();
    }

    private onCaptureSelectedListener listener;

    public void  setOnCaptureSelectedLister(onCaptureSelectedListener listener) {
        this.listener = listener;
    }

    public CaptureFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        listener.onShow();

        Log.d("DS", "CaptureFragment.java onResume");

        progressBar.setProgress(0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_capture, container, false);

        // Add a listener to the Capture button
        final ImageButton captureButton = (ImageButton) view.findViewById(R.id.button_capture);

        final ImageButton viewMapsButton = (ImageButton) view.findViewById(R.id.btnDenuncias);

        viewMapsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickViewMap();
            }
        });

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setProgress(0);

        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        captureButton.setVisibility(View.GONE);
                        viewMapsButton.setVisibility(View.GONE);
                        listener.onClickCapture();

                        ObjectAnimator progressAnimator;
                        progressAnimator = ObjectAnimator.ofInt(progressBar, "progress", 0,100);
                        progressAnimator.setDuration(5000);
                        progressAnimator.start();

                    }
                }
        );

        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

}
