package gad.denunciasalud.fragments;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import gad.denunciasalud.R;


public class CategoryFragment extends Fragment implements View.OnClickListener {

    private TextView textDescripcion;
    private ImageButton btnRuido;
    private ImageButton btnZika;
    private ImageButton btnNext;
    private ImageButton btnOlores;
    private ImageButton btnAgua;
    private ImageButton btnBasura;
    private ImageButton btnHumo;
    private ImageButton btnAtencion;
    private ImageButton btnPlagas;
    private ImageButton btnRestaurante;
    private ImageButton btnProductos;
    private ImageButton btnTabaco;
    private ImageButton btnOtros;
    private ImageButton btnFace;

    public interface CategoryListener{
        void onCategorySelected(String face);
    }

    private CategoryListener listener;

    public void  setCategoryLister(CategoryListener listener){
        this.listener = listener;
    }


    public CategoryFragment(){

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    String category;

    int[] defaultIcons = {
            R.drawable.cat_withe_zika ,
            R.drawable.cat_withe_ruido ,
            R.drawable.cat_withe_aguas ,
            R.drawable.cat_withe_olores ,
            R.drawable.cat_withe_residuos ,
            R.drawable.cat_withe_humo ,
            R.drawable.cat_withe_vectores ,
            R.drawable.cat_withe_atencion ,
            R.drawable.cat_withe_restaurantes ,
            R.drawable.cat_white_productos ,
            R.drawable.cat_withe_fumar ,
            R.drawable.cat_withe_mas
    };

    int[] angryIcons = {
            R.drawable.cat_red_mosquitos,
            R.drawable.cat_red_ruido,
            R.drawable.cat_red_agua,
            R.drawable.cat_red_olores,
            R.drawable.cat_red_residuos,
            R.drawable.cat_red_humo,
            R.drawable.cat_red_vectores,
            R.drawable.cat_red_atencion,
            R.drawable.cat_red_restaurante,
            R.drawable.cat_reed_productos,
            R.drawable.cat_reed_fumar,
            R.drawable.cat_red_mas
    };

    int[] happyIcons = {
            R.drawable.cat_green_mosquitos,
            R.drawable.cat_green_ruido,
            R.drawable.cat_green_agua,
            R.drawable.cat_green_olores,
            R.drawable.cat_green_residuos,
            R.drawable.cat_green_humo,
            R.drawable.cat_green_vectores,
            R.drawable.cat_green_atencion,
            R.drawable.cat_green_restaurante,
            R.drawable.cat_green_productos,
            R.drawable.cat_green_fumar,
            R.drawable.cat_green_mas
    };

    int[] sadIcons = {
            R.drawable.cat_yellow_mosquitos,
            R.drawable.cat_yellow_ruido,
            R.drawable.cat_yellow_agua,
            R.drawable.cat_yellow_olores,
            R.drawable.cat_yellow_residuos,
            R.drawable.cat_yellow_humo,
            R.drawable.cat_yellow_vectores,
            R.drawable.cat_yellow_atencion,
            R.drawable.cat_yellow_restaurante,
            R.drawable.cat_yellow_productos,
            R.drawable.cat_yellow_fumar,
            R.drawable.cat_yellow_mas
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        perception = getArguments().getString("perception");

        View view = inflater.inflate(R.layout.fragment_category, container, false);

        btnFace = (ImageButton) view.findViewById(R.id.btnFace);


        if(perception == "happy"){
            btnFace.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_face_happy, null));
        }
        if(perception == "sad"){
            btnFace.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_face_sad, null));
        }
        if(perception == "angry"){
            btnFace.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_face_angry, null));
        }

        textDescripcion = (TextView) view.findViewById(R.id.textDescripcion);

        btnZika = (ImageButton) view.findViewById(R.id.btnZika);
        btnZika.setOnClickListener(this);

        btnRuido = (ImageButton) view.findViewById(R.id.btnRuido);
        btnRuido.setOnClickListener(this);

        btnAgua = (ImageButton) view.findViewById(R.id.btnAgua);
        btnAgua.setOnClickListener(this);

        btnOlores = (ImageButton) view.findViewById(R.id.btnOlores);
        btnOlores.setOnClickListener(this);


        btnBasura = (ImageButton) view.findViewById(R.id.btnBasura);
        btnBasura.setOnClickListener(this);

        btnHumo = (ImageButton) view.findViewById(R.id.btnHumo);
        btnHumo.setOnClickListener(this);

        btnPlagas = (ImageButton) view.findViewById(R.id.btnPlagas);
        btnPlagas.setOnClickListener(this);

        btnAtencion = (ImageButton) view.findViewById(R.id.btnAtencion);
        btnAtencion.setOnClickListener(this);

        btnRestaurante = (ImageButton) view.findViewById(R.id.btnRestaurante);
        btnRestaurante.setOnClickListener(this);

        btnProductos = (ImageButton) view.findViewById(R.id.btnProductos);
        btnProductos.setOnClickListener(this);

        btnTabaco = (ImageButton) view.findViewById(R.id.btnTabaco);
        btnTabaco.setOnClickListener(this);

        btnOtros = (ImageButton) view.findViewById(R.id.btnOtros);
        btnOtros.setOnClickListener(this);


        btnNext = (ImageButton) view.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);


        return view;
    }

    private void deselectAllButtons(){

        btnZika.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[0], null));
        btnRuido.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[1], null));
        btnAgua.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[2], null));
        btnOlores.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[3], null));
        btnBasura.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[4], null));
        btnHumo.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[5], null));
        btnPlagas.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[6], null));
        btnAtencion.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[7], null));
        btnRestaurante.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[8], null));
        btnProductos.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[9], null));
        btnTabaco.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[10], null));
        btnOtros.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultIcons[11], null));

    }

    private String perception;


    public void onClick(View v) {

        int id = v.getId();

        if(id == R.id.btnNext){
            listener.onCategorySelected(category);
        }else{
            deselectAllButtons();
        }

        int[] drawables = sadIcons;

        if(perception == "happy"){
            drawables = happyIcons;
        }
        if(perception == "sad"){
            drawables = sadIcons;
        }
        if(perception == "angry"){
            drawables = angryIcons;
        }

        if(id == R.id.btnZika){
            category = "mosquitos";
            textDescripcion.setText(getResources().getText(R.string.text_zika));
            btnZika.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[0], null));
        }

        if(id == R.id.btnRuido){
            category = "ruido";
            textDescripcion.setText(getResources().getText(R.string.text_ruido));
            btnRuido.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[1], null));
        }

        if(id == R.id.btnAgua){
            category = "agua";
            textDescripcion.setText(getResources().getText(R.string.text_aguas));
            btnAgua.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[2], null));
        }

        if(id == R.id.btnOlores){
            category = "olores";
            textDescripcion.setText(getResources().getText(R.string.text_olores));
            btnOlores.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[3], null));
        }

        if(id == R.id.btnBasura){
            category = "residuos";
            textDescripcion.setText(getResources().getText(R.string.text_residuos));
            btnBasura.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[4], null));
        }

        if(id == R.id.btnHumo){
            category = "humo";
            textDescripcion.setText(getResources().getText(R.string.text_humos));
            btnHumo.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[5], null));
        }

        if(id == R.id.btnPlagas){
            category = "vectores";
            textDescripcion.setText(getResources().getText(R.string.text_vectores));
            btnPlagas.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[6], null));
        }

        if(id == R.id.btnAtencion){
            category = "atencion";
            textDescripcion.setText(getResources().getText(R.string.text_atencion));
            btnAtencion.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[7], null));
        }

        if(id == R.id.btnRestaurante){
            category = "restaurante";
            textDescripcion.setText(getResources().getText(R.string.text_restaurantes));
            btnRestaurante.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[8], null));
        }

        if(id == R.id.btnProductos){
            category = "productos";
            textDescripcion.setText(getResources().getText(R.string.text_productos));
            btnProductos.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[9], null));
        }

        if(id == R.id.btnTabaco){
            category = "tabaco";
            textDescripcion.setText(getResources().getText(R.string.text_tabaco));
            btnTabaco.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[10], null));
        }

        if(id == R.id.btnOtros){
            category = "otros";
            textDescripcion.setText(getResources().getText(R.string.text_otros));
            btnOtros.setImageDrawable(ResourcesCompat.getDrawable(getResources(), drawables[11], null));
        }



    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


}
