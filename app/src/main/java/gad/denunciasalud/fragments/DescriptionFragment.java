package gad.denunciasalud.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import gad.denunciasalud.R;

public class DescriptionFragment extends Fragment {


    private String perception;
    private ImageButton btnFace;
    private String category;
    private ImageButton btnCategory;

    public interface onSavePostListener{
        void onSavePost(String description, Boolean sendToMS, Boolean sendToFB);
    }

    private onSavePostListener listener;

    public void  setOnSavePostLister(onSavePostListener listener){
        this.listener = listener;
    }

    public DescriptionFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void setProgressBar(int progress){
        progressBar.setProgress(progress);
    }

    private TextView editDescription;
    private ImageButton savePostButton;
    private Switch sendToFB;
    private Switch sendToMS;

    private ProgressBar progressBar;

    private void setCategoryImage(){

        if(category == "mosquitos"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_withe_zika, null));
        }

        if(category == "ruido"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_withe_ruido, null));
        }

        if(category == "olores"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_withe_olores, null));
        }

        if(category == "agua"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_withe_aguas, null));
        }

        if(category == "residuos"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_withe_residuos, null));
        }

        if(category == "humo"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_withe_humo, null));
        }

        if(category == "atencion"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_withe_atencion, null));
        }

        if(category == "vectores"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_withe_vectores, null));
        }

        if(category == "restaurante"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_withe_restaurantes, null));
        }

        if(category == "productos"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_white_productos, null));
        }

        if(category == "tabaco"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_withe_fumar, null));
        }

        if(category == "otros"){
            btnCategory.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.cat_withe_mas, null));
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_description, container, false);

        perception = getArguments().getString("perception");
        category = getArguments().getString("category");


        btnFace = (ImageButton) view.findViewById(R.id.btnFace);
        btnCategory = (ImageButton) view.findViewById(R.id.btnCategory);

        setCategoryImage();


        if(perception == "happy"){
            btnFace.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_face_happy, null));
        }
        if(perception == "sad"){
            btnFace.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_face_sad, null));
        }
        if(perception == "angry"){
            btnFace.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_face_angry, null));
        }

        editDescription = (EditText) view.findViewById(R.id.editDescription);
        savePostButton = (ImageButton) view.findViewById(R.id.btnCheck);

        sendToFB = (Switch) view.findViewById(R.id.switchCompartir);
        sendToMS = (Switch) view.findViewById(R.id.switchAutoridades);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setProgress(0);

        savePostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                savePostButton.setEnabled(false);

                listener.onSavePost(editDescription.getText().toString(), sendToMS.isChecked(), sendToFB.isChecked());
            }
        });

        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

}
