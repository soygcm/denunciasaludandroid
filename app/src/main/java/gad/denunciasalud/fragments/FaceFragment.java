package gad.denunciasalud.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import gad.denunciasalud.R;



public class FaceFragment extends Fragment {


    private ImageButton btnSad;
    private ImageButton btnAngry;

    public void changePlace(CharSequence name) {

        geoText.setText(name);

        faceHappy.setVisibility(View.VISIBLE);
        btnSad.setVisibility(View.VISIBLE);
        btnAngry.setVisibility(View.VISIBLE);

    }

    public interface onFaceSelectedListener{
        void onFaceSelected(String face);

        void onDirectionClick();
    }

    private onFaceSelectedListener listener;

    public void  setOnFaceSelectedLister(onFaceSelectedListener listener){
        this.listener = listener;
    }

    public FaceFragment() {


    }


    private ImageButton faceHappy;

    private LinearLayout directionButton;
    private TextView geoText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_faces, container, false);

        faceHappy = (ImageButton) view.findViewById(R.id.btnHappy);
        faceHappy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onFaceSelected("happy");

            }
        });

        btnSad = (ImageButton) view.findViewById(R.id.btnSad);
        btnSad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onFaceSelected("sad");

            }
        });

        btnAngry = (ImageButton) view.findViewById(R.id.btnAngry);
        btnAngry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onFaceSelected("angry");

            }
        });

        faceHappy.setVisibility(View.GONE);
        btnSad.setVisibility(View.GONE);
        btnAngry.setVisibility(View.GONE);


        directionButton = (LinearLayout) view.findViewById(R.id.idGeo);
        geoText = (TextView) view.findViewById(R.id.geoText);

        directionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onDirectionClick();
            }
        });


        return view;
    }



    @Override
    public void onDetach() {
        super.onDetach();
    }


}
