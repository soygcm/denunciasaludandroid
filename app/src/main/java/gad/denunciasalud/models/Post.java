package gad.denunciasalud.models;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import org.json.JSONObject;

import java.io.Console;
import java.io.File;
import java.util.Arrays;

import gad.denunciasalud.MainActivity;
import gad.denunciasalud.R;

/**
 * Created by gabriel on 3/7/16.
 */
public class Post {

    private String videoURL; 
    private String assetUrl; 
    private String s3Url; 
    private String tempUrl; 

    private String descriptionText; 
    private ParseGeoPoint location;
    private String perception = "sad";

    public String getOs() {
        if(os == null){
            return "";
        }else{
            return os;
        }
    }

    public void setOs(String os) {
        this.os = os;
    }

    private String os = "";

    private String categoryName;
    private ParseObject parseObject;
    private String objectId;
    private boolean active = false;

    private Activity activity;


    private String name = "";
    private String phone = "";
    private String mail = "";
    private boolean confidential = false;

    private boolean shareOnFacebook = false;
    private boolean sendToMS = false;
    private TransferUtility transferUtility;

    private CallbackManager callbackManager;

    private MarkerOptions marker;

    public Post(Bundle extras) {

        if (extras != null) {
            parseObject = ParseObject.createWithoutData("Post", extras.getString("id"));

            objectId = extras.getString("id");
            categoryName = extras.getString("categoryName");
            descriptionText = extras.getString("descriptionText");
            perception = extras.getString("perception");
            os = extras.getString("os");
            active = extras.getBoolean("active");

            Double lat = extras.getDouble("lat");
            Double lng = extras.getDouble("lng");
            location = new ParseGeoPoint(lat, lng);

        }

    }

    public Bundle toBundle(){

        if(objectId != null ){

            Bundle extras = new Bundle();

            extras.putString("id", objectId);
            extras.putString("categoryName", categoryName);
            extras.putString("descriptionText", descriptionText);
            extras.putString("perception", perception);
            extras.putBoolean("active", active);
            extras.putString("os", os);
            extras.putDouble("lat", location.getLatitude());
            extras.putDouble("lng", location.getLongitude());

            return  extras;

        }else{
            return null;
        }
    }

    public Drawable getPerceptionDrawable() {

        if(perception.equals("happy")){
            return ResourcesCompat.getDrawable(activity.getResources(), R.drawable.btn_face_happy, null);
        }
        if(perception.equals("sad")){
            return ResourcesCompat.getDrawable(activity.getResources(), R.drawable.btn_face_sad, null);
        }

        return ResourcesCompat.getDrawable(activity.getResources(), R.drawable.btn_face_angry, null);

    }


    public interface OnPostListener{
//        void onSavePost();
        void onUploadVideo();
        void onDownloadVideo(File file);
        void onPostToFB();
        void onUploadProgressChange(int percentage);
        void onDownloadProgressChange(int percentage);

    }

    private OnPostListener listener;

    public void  setOnPostListener(OnPostListener listener){
        this.listener = listener;
    }


    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void callbackManager(int requestCode, int resultCode, Intent data) {

        if(callbackManager != null)
        {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void prepareAmazonS3() {
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                activity,    /* get the context for the application */
                "us-east-1:f2da8aa3-b1c2-4aca-8a4d-c96956906f20",    /* Identity Pool ID */
                Regions.US_EAST_1          /* Region for your identity pool--US_EAST_1 or EU_WEST_1*/
        );


        // Create an S3 client
        AmazonS3 s3 = new AmazonS3Client(credentialsProvider);

        // Set the region of your S3 bucket
        s3.setRegion(Region.getRegion(Regions.US_EAST_1));

        transferUtility = new TransferUtility(s3, activity);

    }

    public void loginAndPost(){

        if(shareOnFacebook){

        if (AccessToken.getCurrentAccessToken() != null){

            postToFB();

        }else {

            callbackManager = CallbackManager.Factory.create();

            LoginManager.getInstance().logInWithPublishPermissions(activity, Arrays.asList("publish_actions"));

            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {

                            postToFB();

                        }

                        @Override
                        public void onCancel() {
                            Log.d("DenunciaSalud", "onCancel");
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            exception.printStackTrace();
                        }
                    });

        }

        }else{
            listener.onPostToFB();
        }
    }

    public void postToFB() {

        GraphRequest request = GraphRequest.newPostRequest(AccessToken.getCurrentAccessToken(), "me/videos", null, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {

                listener.onPostToFB();

                Log.d("DenunciaSalud", "END?? "+ response.getError());
            }
        });

        Log.d("PostingUrl", "url: "+ s3Url);

        Bundle parameters = request.getParameters();
        parameters.putString("file_url", s3Url);
        parameters.putString("description", descriptionText);
        request.setParameters(parameters);
        request.executeAsync();

    }

    public void savePost(final SaveCallback callback){

        parseObject.put("categoryName", categoryName);
        parseObject.put("description", descriptionText);
        parseObject.put("perception", perception);
        parseObject.put("active", active);
        parseObject.put("fb", shareOnFacebook);
        parseObject.put("ms", sendToMS);
        parseObject.put("name", name);
        parseObject.put("phone", phone);
        parseObject.put("email", mail);
        parseObject.put("confidential", confidential);
        parseObject.put("location", location);
        parseObject.put("os", os);

        parseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    objectId = parseObject.getObjectId();
                }
                callback.done(e);
            }
        });

    }

    public void saveAndUploadVideo(){

        savePost(new SaveCallback() {
            @Override
            public void done(ParseException e) {

                uploadVideo();

            }
        });

    }

    public void downloadVideo(){

        prepareAmazonS3();


        final File file = MainActivity.getOutputMediaFile(MainActivity.MEDIA_TYPE_VIDEO);

        TransferObserver observer = this.transferUtility.download(
                "appdenuncia",
                parseObject.getObjectId()+".mp4",
                file
        );

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if(state == TransferState.COMPLETED){
                    listener.onDownloadVideo(file);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percentage = (int) (bytesCurrent * 100 / (bytesTotal + 0.5) );

                listener.onDownloadProgressChange(percentage);

            }

            @Override
            public void onError(int id, Exception ex) {

            }
        });

    }

    public void uploadVideo(){

        prepareAmazonS3();


        TransferObserver observer = this.transferUtility.upload(
                "appdenuncia",
                objectId+".mp4",
                new File(videoURL),
                CannedAccessControlList.PublicRead
        );

        observer.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                if(state == TransferState.COMPLETED){

                    s3Url = "https://d2vpm3nruwfvqw.cloudfront.net/"+objectId+".mp4";

                    listener.onUploadVideo();

                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//                if()
                int percentage = (int) (bytesCurrent * 100 / (bytesTotal + 0.5) );

                listener.onUploadProgressChange(percentage);
            }

            @Override
            public void onError(int id, Exception ex) {

                ex.printStackTrace();

            }

        });

    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public String getAssetUrl() {
        return assetUrl;
    }

    public void setAssetUrl(String assetUrl) {
        this.assetUrl = assetUrl;
    }

    public String getS3Url() {
        return s3Url;
    }

    public void setS3Url(String s3Url) {
        this.s3Url = s3Url;
    }

    public String getTempUrl() {
        return tempUrl;
    }

    public void setTempUrl(String tempUrl) {
        this.tempUrl = tempUrl;
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    public ParseGeoPoint getLocation() {
        return location;
    }

    public String getPerception() {
        return perception;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public ParseObject getParseObject() {
        return parseObject;
    }

    public void setParseObject(ParseObject parseObject) {
        this.parseObject = parseObject;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public boolean isActive() {
        return active;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getMail() {
        return mail;
    }

    public boolean isConfidential() {
        return confidential;
    }

    public boolean isShareOnFacebook() {
        return shareOnFacebook;
    }

    public boolean isSendToMS() {
        return sendToMS;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setPerception(String perception) {
        this.perception = perception;
    }

    public void setLocation(ParseGeoPoint location) {
        this.location = location;
    }

    public void setDescriptionText(String descriptionText) {
        this.descriptionText = descriptionText;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setShareOnFacebook(boolean shareOnFacebook) {
        this.shareOnFacebook = shareOnFacebook;
    }

    public void setSendToMS(boolean sendToMS) {
        this.sendToMS = sendToMS;
    }

    public void setConfidential(boolean confidential) {
        this.confidential = confidential;
    }

    public void setActive(boolean active) {
        this.active = active;
    }




    public Post(){

        parseObject = new ParseObject("Post");


    }

    public Post(ParseObject parseObject){

        this.parseObject = parseObject;

        this.categoryName = parseObject.getString("categoryName");
        this.descriptionText = parseObject.getString("description");

        this.perception = parseObject.getString("perception");

        this.location = parseObject.getParseGeoPoint("location");

        this.os = parseObject.getString("os");

        this.objectId = parseObject.getObjectId();

    }

    public void setTransferUtility(TransferUtility transferUtility) {
        this.transferUtility = transferUtility;
    }

    public TransferUtility getTransferUtility() {
        return transferUtility;
    }

    public MarkerOptions getMarker() {



        LatLng latLng = new LatLng(this.location.getLatitude(), this.location.getLongitude());

        int height = 40;
        int width = 40;

        BitmapDrawable bitmapdraw = (BitmapDrawable) ResourcesCompat.getDrawable(activity.getResources(), R.drawable.icon_point_yellow, null);

        if(this.perception.equals("happy")){
            bitmapdraw = (BitmapDrawable) ResourcesCompat.getDrawable(activity.getResources(), R.drawable.icon_point_green, null);
        }
        if(this.perception.equals("angry")){
            bitmapdraw = (BitmapDrawable) ResourcesCompat.getDrawable(activity.getResources(), R.drawable.icon_point_red, null);
        }

        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        this.marker =  new MarkerOptions()
                .position(latLng)
                .title(descriptionText)
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker));

        return marker;
    }

    int[] angryIcons = {
            R.drawable.cat_red_mosquitos,
            R.drawable.cat_red_ruido,
            R.drawable.cat_red_agua,
            R.drawable.cat_red_olores,
            R.drawable.cat_red_residuos,
            R.drawable.cat_red_humo,
            R.drawable.cat_red_vectores,
            R.drawable.cat_red_atencion,
            R.drawable.cat_red_restaurante,
            R.drawable.cat_reed_productos,
            R.drawable.cat_reed_fumar,
            R.drawable.cat_red_mas
    };

    int[] happyIcons = {
            R.drawable.cat_green_mosquitos,
            R.drawable.cat_green_ruido,
            R.drawable.cat_green_agua,
            R.drawable.cat_green_olores,
            R.drawable.cat_green_residuos,
            R.drawable.cat_green_humo,
            R.drawable.cat_green_vectores,
            R.drawable.cat_green_atencion,
            R.drawable.cat_green_restaurante,
            R.drawable.cat_green_productos,
            R.drawable.cat_green_fumar,
            R.drawable.cat_green_mas
    };

    int[] sadIcons = {
            R.drawable.cat_yellow_mosquitos,
            R.drawable.cat_yellow_ruido,
            R.drawable.cat_yellow_agua,
            R.drawable.cat_yellow_olores,
            R.drawable.cat_yellow_residuos,
            R.drawable.cat_yellow_humo,
            R.drawable.cat_yellow_vectores,
            R.drawable.cat_yellow_atencion,
            R.drawable.cat_yellow_restaurante,
            R.drawable.cat_yellow_productos,
            R.drawable.cat_yellow_fumar,
            R.drawable.cat_yellow_mas
    };

    public Drawable getCategoryDrawable(){


        int[] drawables = sadIcons;

        if(this.perception.equals("happy")){
            drawables = happyIcons;
        }
        if(this.perception.equals("angry")){
            drawables = angryIcons;
        }
        
        if(this.categoryName.equals("ruido")){
            return ResourcesCompat.getDrawable(activity.getResources(), drawables[1], null);
        }

        if(this.categoryName.equals("agua")){
            return ResourcesCompat.getDrawable(activity.getResources(), drawables[2], null);
        }

        if(this.categoryName.equals("olores")){
            return ResourcesCompat.getDrawable(activity.getResources(), drawables[3], null);
        }

        if(this.categoryName.equals("residuos")){
            return ResourcesCompat.getDrawable(activity.getResources(), drawables[4], null);
        }

        if(this.categoryName.equals("humo")){
            return ResourcesCompat.getDrawable(activity.getResources(), drawables[5], null);
        }

        if(this.categoryName.equals("vectores")){
            return ResourcesCompat.getDrawable(activity.getResources(), drawables[6], null);
        }

        if(this.categoryName.equals("atencion")){
            return ResourcesCompat.getDrawable(activity.getResources(), drawables[7], null);
        }

        if(this.categoryName.equals("restaurante")){
            return ResourcesCompat.getDrawable(activity.getResources(), drawables[8], null);
        }

        if(this.categoryName.equals("productos")){
            return ResourcesCompat.getDrawable(activity.getResources(), drawables[9], null);
        }

        if(this.categoryName.equals("tabaco")){
            return ResourcesCompat.getDrawable(activity.getResources(), drawables[10], null);
        }

        if(this.categoryName.equals("otros")){
            return ResourcesCompat.getDrawable(activity.getResources(), drawables[11], null);
        }

        return ResourcesCompat.getDrawable(activity.getResources(), drawables[0], null);


    }

}
